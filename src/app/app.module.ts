import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CHeader } from './header/header';
import { CPortada } from './portada/portada';
import { CFooter } from './footer/footer';

@NgModule({
  declarations: [
    AppComponent,
    CHeader,
    CPortada,
    CFooter
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
